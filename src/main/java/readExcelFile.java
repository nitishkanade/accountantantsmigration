import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

public class readExcelFile {

    private final migrationMainConnectionClass migrationMainConnectionClass = new migrationMainConnectionClass();

    public void readexcelfile(String filepath, Connection connection,boolean clientExecute,boolean clientLoginExecute, boolean sectorReference) throws IOException, SQLException {
        System.out.println("EXCEL FILE :" + filepath);
        FileInputStream excelFile = new FileInputStream(new File(filepath));

        Workbook workbook = new HSSFWorkbook(excelFile);
        Sheet sheet = workbook.getSheetAt(0);
        DataFormatter formatter = new DataFormatter();

        System.out.println();

        Iterator<Row> iterator = sheet.rowIterator();

        while (iterator.hasNext()) {
            Row currentRow = iterator.next();
            Iterator<Cell> cellIterator = currentRow.cellIterator();
            HashMap<String, String> columnNameValue = new LinkedHashMap<>();
            int rowNum = currentRow.getRowNum();

            //bcz first two entries are just test
            if (rowNum > 1) {
                int companyNumber = 0;
                int clientNo = -1;
                String companyName = "";
                String Address1 = "";
                String Address2 = "";
                String Address3 = "";
                String Address4 = "";
                String Postcode = "";
                String email = "";



                for (int i = 0; i < 9; i++) {
                    companyNumber = (int) currentRow.getCell(0).getNumericCellValue();
                    clientNo = (int) currentRow.getCell(1).getNumericCellValue();
                    companyName = formatter.formatCellValue(currentRow.getCell(2));
                    Address1 = formatter.formatCellValue(currentRow.getCell(3));
                    Address2 = formatter.formatCellValue(currentRow.getCell(4));
                    Address3 = formatter.formatCellValue(currentRow.getCell(5));
                    Address4 = formatter.formatCellValue(currentRow.getCell(6));
                    Postcode = formatter.formatCellValue(currentRow.getCell(7));
                    email = formatter.formatCellValue(currentRow.getCell(8));

                    columnNameValue.put("companyNumber", String.valueOf(companyNumber));
                    columnNameValue.put("clientNo", String.valueOf(clientNo));
                    columnNameValue.put("companyName", companyName);
                    columnNameValue.put("Address1", Address1);
                    columnNameValue.put("Address2", Address2);
                    columnNameValue.put("Address3", Address3);
                    columnNameValue.put("Address4", Address4);
                    columnNameValue.put("postcode", Postcode);
                    columnNameValue.put("email", email);


                }

                //Client entries made
                if(clientExecute) {
                    insertValuesInLocktonPf_client(columnNameValue, connection);
                }

                //create login for each entry that has a email and not duplicate
                if(clientLoginExecute) {
                    insertValuesInLocktonPf_client_login(columnNameValue, connection);
                }

                //import sector reference Data
                //companynumber
                if(sectorReference) {
                    insertValuesInSectorReferenceTable(columnNameValue,connection);
                }


            }
        }
    }

    private void insertValuesInSectorReferenceTable(HashMap<String, String> columnNameValue, Connection connection) throws SQLException {
        int clientId = getClientIdForClientName(columnNameValue.get("companyName"),connection);
        if(clientId>0) {

            StringBuilder queryInsertForSectorReference = new StringBuilder();
            queryInsertForSectorReference.append("INSERT INTO locktonPf_sector_reference (sectorAccountNumber,sectorCompanyNumber,clientId,reference) VALUES (");
            queryInsertForSectorReference.append(columnNameValue.get("clientNo")+",");
            queryInsertForSectorReference.append(columnNameValue.get("companyNumber")+",");
            queryInsertForSectorReference.append(clientId+",");
            queryInsertForSectorReference.append("\"accountants import\"");
            queryInsertForSectorReference.append(")");
            queryInsertForSectorReference.append(" ON DUPLICATE KEY UPDATE reference=CONCAT_WS(',',reference,'duplicate from accountants company Name "+columnNameValue.get("companyName").replaceAll("\'","")+"') ");
            Statement statement = connection.createStatement();
            statement.executeUpdate(queryInsertForSectorReference.toString());

        } else {
            System.out.println(columnNameValue.get("companyName")+" , ClientNo : "+columnNameValue.get("clientNo") + ", CompanyNumber:" + columnNameValue.get("companyNumber")+",Email:"+columnNameValue.get("email"));
        }

    }


    public void insertValuesInLocktonPf_client(HashMap<String, String> columnNameValues, Connection connection) throws SQLException {

//        System.out.println(columnNameValues.get("email"));

        if(!columnNameValues.get("email").isEmpty()) {

            if(!columnNameValues.get("email").equals("n/a")) {
                if (!checkIfClientExsits(columnNameValues.get("email"), connection) && columnNameValues.get("email").contains("@")) {
                    //accountants site id 3

                    //password : LocktonRenewal.18 : $2a$11$HuG0VoML2f1GZcv0BqmGROGc7hCSIRWkPeKMNp/QkI2N1FUe2TjjC

                    //Locton.1 : $2a$11$zxZ/CIw4E4HS9TCei93IOegMrEj5gCmjIsOxY746gN713VR6v/JUe
                    //$2a$11$zxZ/CIw4E4HS9TCei93IOegMrEj5gCmjIsOxY746gN713VR6v/JUe
                    //$2a$11$zxZ/CIw4E4HS9TCei93IOegMrEj5gCmjIsOxY746gN713VR6v/JUe

                    StringBuilder insertClientLoginEntryQuery = new StringBuilder();

                    insertClientLoginEntryQuery.append("INSERT INTO locktonPf_client (email,password,companyName,addressLine1,addressLine2,addressLine3,postcode,primarySiteID,primarySiteIdImport) VALUES (");

                    insertClientLoginEntryQuery.append("\"" + columnNameValues.get("email") + "\"");

                    insertClientLoginEntryQuery.append(",\"" + "$2a$11$HuG0VoML2f1GZcv0BqmGROGc7hCSIRWkPeKMNp/QkI2N1FUe2TjjC\"");
                    insertClientLoginEntryQuery.append(",\"" + StringEscapeUtils.escapeJava(columnNameValues.get("companyName")) + "\"");
                    insertClientLoginEntryQuery.append(",\"" + StringEscapeUtils.escapeJava(columnNameValues.get("Address1")) + "\"");
                    insertClientLoginEntryQuery.append(",\"" + StringEscapeUtils.escapeJava(columnNameValues.get("Address2")) + "\"");
                    insertClientLoginEntryQuery.append(",\"" + StringEscapeUtils.escapeJava(columnNameValues.get("Address3")) + " " + StringEscapeUtils.escapeJava(columnNameValues.get("Address4")) + "\"");
                    insertClientLoginEntryQuery.append(",\"" + StringEscapeUtils.escapeJava(columnNameValues.get("postcode")) + "\"");
                    insertClientLoginEntryQuery.append("," + "3");
                    insertClientLoginEntryQuery.append("," + "3");
                    insertClientLoginEntryQuery.append(")");

                    Statement statement = connection.createStatement();
                    statement.executeUpdate(insertClientLoginEntryQuery.toString());

                } else {
                    System.out.println();
                    System.out.println("Duplicate entry or not valid email");
                    System.out.print("CompanyNumber:"+columnNameValues.get("companyNumber"));
                    System.out.print(",Email:"+columnNameValues.get("email"));
                    System.out.print(",Client No:"+columnNameValues.get("clientNo"));
                    System.out.print(",Company Name:"+columnNameValues.get("companyName"));
                    System.out.println();
                }
            } else {
                System.out.println();
                System.out.println("Email n/a");
                System.out.print("CompanyNumber:"+columnNameValues.get("companyNumber"));
                System.out.print(",Email:"+columnNameValues.get("email"));
                System.out.print(",Client No:"+columnNameValues.get("clientNo"));
                System.out.print(",Company Name:"+columnNameValues.get("companyName"));
                System.out.println();
            }

        } else {
            System.out.println();
            System.out.println("Email Empty");
            System.out.print("CompanyNumber:"+columnNameValues.get("companyNumber"));
            System.out.print(",Email:"+columnNameValues.get("email"));
            System.out.print(",Client No:"+columnNameValues.get("clientNo"));
            System.out.print(",Company Name:"+columnNameValues.get("companyName"));
            System.out.println();
        }
    }

    private boolean checkIfClientExsits(String email, Connection connection) {
            int count = 0;

            String queryForDuplicate = "SELECT COUNT(*) as countDuplicateEntry FROM locktonPf_client WHERE email=\"" + email + "\"";

            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(queryForDuplicate);
                while (resultSet.next()) {
                    count = resultSet.getInt("countDuplicateEntry");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if (count > 0) {
                return true;
            }

            return false;
    }

    private void insertValuesInLocktonPf_client_login(HashMap<String, String> columnNameValue, Connection connection) throws SQLException {
        //client login entries only if email is present
        if(!columnNameValue.get("email").isEmpty()) {
            if(!columnNameValue.get("email").equalsIgnoreCase("n/a")) {
                if(!checkIfClientLoginExists(columnNameValue.get("email"),connection) && columnNameValue.get("email").contains("@")) {
                    //email present and not duplicate so make new entry in client login table
                    StringBuilder insertQueryForclientLogin = new StringBuilder();
                    int clientId = getClientIdForClientName(columnNameValue.get("companyName"),connection);
                    if(clientId > 0) {
                        //client present
                        insertQueryForclientLogin.append("INSERT INTO locktonPf_client_login (email,password,clientID,primarySiteIdImported) VALUES(");
                        insertQueryForclientLogin.append("\""+columnNameValue.get("email")+"\",");
                        insertQueryForclientLogin.append("\"$2a$11$HuG0VoML2f1GZcv0BqmGROGc7hCSIRWkPeKMNp/QkI2N1FUe2TjjC\",");
                        insertQueryForclientLogin.append(clientId+",");
                        insertQueryForclientLogin.append(3);
                        insertQueryForclientLogin.append(")");

                        Statement statement = connection.createStatement();
                        statement.executeUpdate(insertQueryForclientLogin.toString());

                    }
                } else {
                    System.out.println("Email Duplicate or Not a Valid Email");
                    System.out.println();
                    System.out.println("Email n/a");
                    System.out.print("CompanyNumber:"+columnNameValue.get("companyNumber"));
                    System.out.print(",Email:"+columnNameValue.get("email"));
                    System.out.print(",Client No:"+columnNameValue.get("clientNo"));
                    System.out.print(",Company Name:"+columnNameValue.get("companyName"));
                    System.out.println();
                }
            } else {
                //email n/a
            }

        } else {
            //email empty
        }
    }

    private boolean checkIfClientLoginExists(String email,Connection connection) throws SQLException {
        int count = 0;
        String queryForDuplicateClientLogin = "SELECT COUNT(*) AS countDuplicateEntries FROM locktonPf_client_login WHERE email=\""+email+"\"";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(queryForDuplicateClientLogin);
        while (resultSet.next()) {
            count = resultSet.getInt("countDuplicateEntries");
        }
        if(count > 0) {
            return true;
        }
        return false;
    }

    private int getClientIdForClientName(String clientName,Connection connection) throws SQLException {
        int clientId = 0;
        String queryForClientId = "SELECT id FROM locktonPf_client WHERE companyName=\""+clientName+"\" AND primarySiteIdImport=3 LIMIT 1";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(queryForClientId);
        while (resultSet.next()) {
            clientId = resultSet.getInt("id");
        }
        return clientId;
    }

    public void addPrimaryContactRoleToImportedRoles(Connection connectionToLocalLocktonDB) throws SQLException {
//        INSERT INTO locktonPf_contact_client_relation (contactRoleID,clientLoginId,clientID,uniqueToClient)
//        SELECT 21 as contactRoleID, cll.id as clientLoginId, cl.id AS clientID, 1 AS uniqueToClient from locktonPf_client_login AS cll
//        INNER JOIN locktonPf_client AS cl ON cl.id = cll.clientID
//        WHERE primarySiteID=3 AND primarySiteIdImport=3
//        ON DUPLICATE KEY UPDATE duplicateLoginIds = CONCAT_WS(',',duplicateLoginIds,cll.id),updated=NOW()

//        SELECT CONCAT(cll.email,',',cl.companyName,':',duplicateLoginIds) from locktonPf_client_login as cll
//        INNER JOIN locktonPf_client AS cl ON cl.id = cll.clientID
//        INNER JOIN locktonPf_contact_client_relation AS clr ON clr.clientLoginID = cll.id
//        WHERE duplicateLoginIds IS NOT NULL AND clr.contactRoleID = 21 ;
//
//
//        SELECT CONCAT(cll.email,',',cl.companyName,':'),cll.id from locktonPf_client_login as cll
//        INNER JOIN locktonPf_client AS cl ON cl.id = cll.clientID
//        WHERE  cll.id IN (19254,19308,19502,19553,19717,19876,20538,20612,20705,20952,21052,21084,21132,21260,21328,21702,21703,22689,22923,22924,22943,23204,23274,23434,23719,23718)


    }
}
