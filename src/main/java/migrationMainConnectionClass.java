import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class migrationMainConnectionClass {

    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your MySQL JDBC Driver?");
            e.printStackTrace();
            return;
        }

        System.out.println("MySQL JDBC Driver Registered!");
        Connection connectionToLocalLocktonDB = null;

        try {

            connectionToLocalLocktonDB = DriverManager.getConnection("jdbc:mysql://localhost:3306/cms","root", "");
//            connectionToLocalLocktonDB = DriverManager.getConnection("jdbc:mysql://194.116.175.25/cms?useSSL=false","stunn", "r00tBooy3333498FDGDertt5443tuoicrtyNEWSTUNNsdfsew3m1345nv3htk");

            readExcelFile readExcelFile = new readExcelFile();
            boolean clientExecute = false;
            boolean clientLoginExecute = false;
            boolean sectorReference = true;

            readExcelFile.readexcelfile("/Users/nitish/Clients/accountantantsMigration/src/main/java/ACCASectorData.xls",connectionToLocalLocktonDB,clientExecute,clientLoginExecute,sectorReference);


            //add primary contact to imported logins
            readExcelFile.addPrimaryContactRoleToImportedRoles(connectionToLocalLocktonDB);



        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
